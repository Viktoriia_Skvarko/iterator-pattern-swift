import Foundation

// MARK: - Model One Driver

class Driver {
    let isGoodDriver: Bool
    let name: String
    
    init(isGood: Bool, name: String) {
        self.isGoodDriver = isGood
        self.name = name
    }
}


// MARK: - Array Drivers

class Car {
    // свойство чтобы перебирать коллекцию
    var goodDriveIterator: GoodDriverIterator {
        return GoodDriverIterator(drivers: drivers)
    }
    
    // let drivers будет использоваться только в этом классе, поэтому private
    private let drivers = [Driver(isGood: true, name: "Nick"),
                           Driver(isGood: false, name: "Mark"),
                           Driver(isGood: true, name: "Morgan"),
                           Driver(isGood: false, name: "Mary")]
}


// MARK: - Extensions Car (Array Drivers)

// Требование протокола Sequence - создание итератора (makeIterator)
extension Car: Sequence {
    func makeIterator() -> GoodDriverIterator {
        return GoodDriverIterator(drivers: drivers)
    }
}


// MARK: - Creating Iterator GoodDriver

// IteratorProtocol стандатртный класс для создания итератора
// Создание отдельного класса итератора:

class GoodDriverIterator: IteratorProtocol {
    private let drivers: [Driver]
    private var current = 0
    
    init(drivers: [Driver]) {
        self.drivers = drivers.filter{ $0.isGoodDriver }  // фильтр по свойству isGoodDriver = true
    }
    
    // обязательный метод next:
    func next() -> Driver? {
        defer { current += 1}  // defer действие в самом конце
        return drivers.count > current ? drivers[current] : nil
    }
    
    func allDrivers() -> [Driver] {
        return drivers
    }
}


// MARK: - Check Work

let car = Car()
let goodDriverIterator = car.goodDriveIterator.next()

for driver in car {
    print("Good driver:", driver.name)
}
